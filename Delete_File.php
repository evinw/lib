<?php
/**
 * @author Evin Weissenberg
 */
class Delete_File {

    private $file_path;

    /**
     * @param $file_path
     * @return Delete_File
     */
    function setFilePath($file_path) {

        $this->$file_path = (string)$file_path;

        return $this;

    }

    /**
     * @return bool
     */
    function delete() {

        if (unlink($this->file_path)) {

            return true;

        } else {

            return false;

        }
    }

    function __destructor() {

        unset($this->file_path);

    }
}

//$d = new Delete_File();
//$d->setFilePath('/myFile.csv')->delete();