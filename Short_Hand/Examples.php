<?php

//Outputs $a if $a is true
//$a && print $a;

//Create Object From Array
//$object = (object) array ('user1' => 'Franz', 'superuser' => 'ZeusCool');
//echo $object->superuser; // prints out ZeusCool

//Example of a shopping cart implementation of shorthand operator
//print "Your have selected " . $itemsNumber . " item" . ($itemsNumber != 1 ? 's' : '') . ".";

//Useful for development between staging and production
//error_reporting($PRODUCTION ? 0 : E_ALL);

//Short php print tag embeded in html:
//<div><?=$string//?//><!--</div> is the same as <div>--><?php //print $string; ?//><!--</div>-->

//
//echo (true?'true':false?'t':'f'); // outputs t
// This can be more obvious written as
//var_dump(((true ? 'true' : false) ? 't' : 'f'));

$var='';

(true?'isset':'not set');

echo $var;

//echo 0 ?: 1 ?: 2 ?: 3; //1
//echo 1 ?: 0 ?: 3 ?: 2; //1
//echo 2 ?: 1 ?: 0 ?: 3; //2
//echo 3 ?: 2 ?: 1 ?: 0; //3

//echo 0 ?: 1 ?: 2 ?: 3; //1
//echo 0 ?: 0 ?: 2 ?: 3; //2
//echo 0 ?: 0 ?: 0 ?: 3; //3

//for ($i=0; $i<5; $i++) {
//    echo $i == 0 ? 'first_row' : ($i == 4 ? 'last_row' : 'none');
//}