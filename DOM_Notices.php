<?php
/**
 * @author Evin Weissenberg
 */
class DOM_Notice {

    private $message;
    private $bg_color;
    private $text_color;

    /**
     * @param $bg_color
     * @return DOM_Notice
     */
    function setMessageBGColor($bg_color) {

        $this->bg_color = (string)$bg_color;
        return $this;

    }

    /**
     * @param $text_color
     * @return DOM_Notice
     */
    function setMessageTextColor($text_color) {

        $this->text_color = (string)$text_color;
        return $this;

    }

    /**
     * @param $message
     * @return DOM_Notice
     */
    function setMessage($message) {

        $this->message = (string)$message;
        return $this;

    }


    /**
     * @param $property
     * @return mixed
     */
    function __get($property) {

        return $this->$property;

    }


    /**
     * @return string
     */
    function display() {

        $notice = "<div style='background-color: " . $this->bg_color . "; color: ".$this->text_color.";
        padding: 10px; font-weight: bold; text-transform:
        uppercase'>" . $this->message . "</div>";

        return $notice;

    }
}

