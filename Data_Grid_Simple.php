<?php

class Data_Grid_Simple {

    private $table;
    private $row_count;
    private $query;
    private $header_titles = array();
    private $edit;
    private $delete;
    private $column;
    private $limit = 10;
    private $query_data;
    private $order = 'DESC';
    private $primary_id = 'id';
    private $primary_id_value;

    /**
     *
     */
    function __constructor() {

        //Connection goes here

    }

    /**
     * @param $table
     * @return Data_Grid_Simple
     */
    function setTable($table) {

        $this->table = (string)$table;
        return $this;

    }

    /**
     * @param $query
     * @return Data_Grid_Simple
     */
    function setQuery($query) {

        $this->query = (string)$query;
        return $this;

    }

    /**
     * @param $query_data
     * @return Data_Grid_Simple
     */
    function setQueryData($query_data) {

        $this->query_data = (array)$query_data;
        return $this;

    }

    /**
     * @param $titles
     * @return Data_Grid_Simple
     */
    function setHeaderTitles($titles) {

        $this->header_titles = (array)$titles;
        return $this;
    }

    /**
     * @return array
     */
    function runQuery() {

        $array_data = array();

        $result = mysql_query('SELECT * FROM ' . $this->table . ' ORDER by ' . $this->primary_id . ' ' . $this->order . '');

        while ($loop = mysql_fetch_assoc($result)) {

            array_push($data, $loop);

        }

        return $array_data;
    }

    /**
     * @todo not complete
     */
    function renderTable() {

        //Unit Test
        $this->query_data = $data = array(
            array('id' => '55', 'name' => 'John', 'last' => 'Doe', 'email' => 'm@m.com'),
            array('id' => '59', 'name' => 'Ryan', 'last' => 'Belkin', 'email' => 'ryan@tom.com'),
            array('id' => '78', 'name' => 'Fred', 'last' => 'Johnson', 'email' => 'fred@fred.com')
        );
        //

        //Unit Test
        $this->header_titles = $data = array('', 'id', 'Name', 'Last Name', 'Email');
        //
        echo '<table>';

        echo '<tr><td colspan="5"><input name="create-new" value="Create New" type="submit"></td></tr>';

        //Header Titles
        echo '<tr>';
        foreach ($this->header_titles as $key => $value) {

            echo '<td>';

            echo '<b>' . $value . '</b>';

            echo '</td>';

        }
        echo '<tr>';
        //
        $row = 0;
        foreach ($this->query_data as $array) {
            $row++;
            //Data Array Iteration
            echo '<tr>';
            echo '<td><input name="multiple" type="checkbox"></td>';


            foreach ($array as $key => $value) {

                echo '<td style="padding: 5px;">';

                echo $value;

                echo '</td>';

                if ($key == $this->primary_id) {
                    $this->primary_id_value = $value;
                }

            }

            echo '<td><a href="edit.php?id=' . $this->primary_id_value . '">Edit</a></td>';
            echo '<td><a href="delete.php?id=' . $this->primary_id_value . '">Delete</a></td>';

            echo '</tr>';
        }

        echo '<tr><td colspan="5"><input name="delete" value="Delete" type="submit"></td></tr>';
        echo '<tr><td colspan="5"><i>' . $row . ' rows</i></td></tr>';
        echo '</table>';

        //Set
        $this->row_count = $row;
    }

    /**
     *
     */
    function __destructor() {

        unset($this->table);
        unset($this->row_count);
        unset($this->query);

    }
}