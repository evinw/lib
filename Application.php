<?php
/**
 * @author Evin Weissenberg
 */
class Application {

    //APPLICATION INFORMATION
    public static $site_title = 'Starz Global';
    public static $author = 'Midnight Oil Creative';
    public static $address = "3800 West Vanowen Street Burbank, Ca 91505";
    public static $domain_production = "starzglobal.com";
    public static $domain_staging = 'home.lagraphico.com';
    public static $domain_development = 'dev.starzglobal.com';
    public static $time_zone = "America/Los_Angeles";
    public static $images_path = '/assets/images';
    public static $video_path = '/assets/video';
    public static $css_path = '/assets/css';
    public static $css_mobile_path = '/assets/css/mobile';
    public static $javascript_path = '/assets/js';
    public static $logo = "logo.png";
    public static $max_upload = "5242880"; // 5mb
    public static $application_dir = ''; // Application directory

    //
    //DATABASE INFORMATION
    //

    //PRODUCTION
    public static $db_host_production = '';
    public static $db_username_production = '';
    public static $db_password_production = '';
    public static $db_name_production = '';
    public static $db_port_production = '';

    //STAGING
    public static $db_host_staging = '184.107.227.90';
    public static $db_username_staging = 'falling_starz';
    public static $db_password_staging = 'b00gers';
    public static $db_name_staging = 'starz';
    public static $db_port_staging = '3306';

    //DEVELOPMENT
    public static $db_host_development = 'localhost';
    public static $db_username_development = 'root';
    public static $db_password_development = 'b00gers';
    public static $db_name_development = 'starzglobal';
    public static $db_port_development = '3306';

    //ACTIVE CONNECTION (this detects what is the current connection.)
    public $db_host_active;
    public $db_username_active;
    public $db_password_active;
    public $db_database_name_active;
    public $db_port_active;

    /**
     *
     */
    function __construct() {

        if ($_SERVER['HTTP_HOST'] == self::$domain_production) {

            error_reporting(0);

            //Database Connection
            mysql_connect(self::$db_host_production, self::$db_username_production, self::$db_password_production) or die(mysql_error());
            mysql_select_db(self::$db_name_production) or die(mysql_error());

            //Set Production Active
            $this->db_host_active = self::$db_host_production;
            $this->db_username_active = self::$db_username_production;
            $this->db_password_active = self::$db_password_production;
            $this->db_port_active = self::$db_port_production;
            $this->db_database_name_active = self::$db_name_production;

        } elseif ($_SERVER['HTTP_HOST'] == self::$domain_staging) {

            ini_set('display_errors', TRUE);
            error_reporting(E_PARSE);

            //Database Connection
            mysql_connect(self::$db_host_staging, self::$db_username_staging, self::$db_password_staging) or die(mysql_error());
            mysql_select_db(self::$db_name_staging) or die(mysql_error());

            //Set Staging Active
            $this->db_host_active = self::$db_host_staging;
            $this->db_username_active = self::$db_username_staging;
            $this->db_password_active = self::$db_password_staging;
            $this->db_port_active = self::$db_port_staging;
            $this->db_database_name_active = self::$db_name_staging;

        } elseif ($_SERVER['HTTP_HOST'] == self::$domain_development) {

            ini_set('display_errors', TRUE);
            error_reporting(E_ERROR | E_CORE_ERROR | E_COMPILE_ERROR);

            //Database Connection
            mysql_connect(self::$db_host_development, self::$db_username_development, self::$db_password_development) or die(mysql_error());
            mysql_select_db(self::$db_name_development) or die(mysql_error());

            //Set Development Active
            $this->db_host_active = self::$db_host_development;
            $this->db_username_active = self::$db_username_development;
            $this->db_password_active = self::$db_password_development;
            $this->db_port_active = self::$db_port_development;
            $this->db_database_name_active = self::$db_name_development;

        } else {
            error_reporting(0);
        }
        ob_start();
        session_start();
        ini_set('default_charset', 'UTF-8');
        define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT']);
        date_default_timezone_set(self::$time_zone);
        ini_set('auto_detect_line_endings', TRUE);
        ini_set('memory_limit', '16M');

        if (!ini_get('safe_mode')) {
            set_time_limit(25);
        }
    }

    /**
     * @static
     * @return bool
     */
    public static function developmentEnvironment() {

        if ($_SERVER['HTTP_HOST'] == self::$domain_development || $_SERVER['HTTP_HOST'] == self::$domain_staging) {

            error_reporting(E_ALL | E_WARNING | E_NOTICE);

            echo "<style type='text/css'> .debug {font-size: 12px; background-color: black; color: white; font-family: arial, Helvetica,sans-serif;  padding: 10px;} </style>";

            echo "<div style='background-color: red; color: white; font-weight: bolder; padding: 10px;'>DEVELOPMENT / STAGING ENVIRONMENT</div>";
            ini_set("display_errors", 1);
            echo "<div class='debug'> Author: Evin Weissenberg 2011 - " . date('Y') . ". http://www.evinw.com</div>";
            echo "<div class='debug'>PHP Version: " . PHP_VERSION . "</div>";
            echo "<div class='debug'>Request URI: " . $_SERVER['REQUEST_URI'] . "</div>";
            echo "<div class='debug'>Document Root: " . $_SERVER['DOCUMENT_ROOT'] . "</div>";
            echo "<div class='debug'>Server IP: " . $_SERVER['SERVER_ADDR'] . "</div>";
            echo "<div class='debug'>My IP: " . $_SERVER['REMOTE_ADDR'] . "</div>";
            echo "<div class='debug'>Request Method: : " . $_SERVER['REQUEST_METHOD'] . "</div>";
            echo "<div class='debug'>Query String: " . $_SERVER['QUERY_STRING'] . "</div>";
            echo "<div class='debug'>Local Port: " . $_SERVER['REMOTE_PORT'] . "</div>";
            echo "<div class='debug'>Server Software: " . $_SERVER['SERVER_SOFTWARE'] . "</div>";
            echo "<div class='debug'>Contents of the Connection: " . $_SERVER['HTTP_CONNECTION'] . "</div>";
            echo "<div class='debug'>Server Time Zone: " . date_default_timezone_get() . "</div>";

            print('<pre>');
            print_r($_REQUEST);
            print('</pre>');

            return TRUE;

        } else {

            // $production = self::productionEnvironment();

        }

    }


    /**
     * @static
     *
     */
    public static function css() {

        $browser = $_SERVER['HTTP_USER_AGENT'];
        $find = strstr($browser, "Mobile");

        if ($find == TRUE) {
            echo "<link rel='stylesheet' href=" . DOC_ROOT . "'/assets/css/mobile.css'>";
        } else {
            echo "<link rel='stylesheet' href=" . DOC_ROOT . "'/assets/css/style.css'>";
        }
    }

    /**
     * @static
     *
     */
    public static function js() {

        echo "<script type='text/javascript' src='" . DOC_ROOT . "/assets/js/main.js'></script>";
        echo "<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js'></script>";
    }

    /**
     * @static
     *
     */
    public static function seo() {

    }

    /**
     * @static
     * @return string
     */
    public static function todayDate() {

        $date = date('Y-m-d');
        return $date;

    }
}