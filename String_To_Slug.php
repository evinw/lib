<?php
/**
 * @author Evin Weissenberg
 */
class String_To_Slug {

    private $string;

    /**
     * @param $string
     * @return Convert_String_To_Slug
     */
    public function setString($string) {

        $this->string = (string)$string;
        return $this;

    }

    /**
     * @return mixed
     */
    function slug() {

        $this->string = strtolower(trim($this->string));
        $this->string = preg_replace('/[^a-z0-9-]/', '-', $this->string);
        $this->string = preg_replace('/-+/', "-", $this->string);

        return $this->string;

    }

    /**
     *
     */
    function __destruct() {

        unset($this->string);

    }
}