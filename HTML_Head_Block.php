<?php
/**
 * @author Evin Weissenberg
 */
class HTML_Head_Block {

    private $author = 'Evin Weissenberg';
    private $title = 'My Cool Site';
    private $description = 'This is the description';
    private $address = 'Los Angeles, Ca';
    private $keywords = array();
    private $revisit = 7; // in days
    private $robot = 'INDEX,FOLLOW';
    private $country = 'USA';

    /**
     * @param $author
     * @return HTML_Head_Block
     */
    function setAuthor($author) {

        $this->author = (string)$author;
        return $this;
    }

    /**
     * @param $title
     * @return HTML_Head_Block
     */
    function setTitle($title) {

        $this->title = (string)$title;
        return $this;
    }

    /**
     * @param $robot
     * @return HTML_Head_Block
     */
    function setRobot($robot) {

        $this->robot = (string)$robot;
        return $this;
    }

    /**
     * @param $description
     * @return HTML_Head_Block
     */
    function setDescription($description) {

        $this->description = (string)$description;
        return $this;
    }

    /**
     * @param $address
     * @return HTML_Head_Block
     */
    function setAddress($address) {

        $this->address = (string)$address;
        return $this;
    }

    /**
     * @param $revisit
     * @return HTML_Head_Block
     */
    function setRevisit($revisit) {

        $this->revisit = (string)$revisit;
        return $this;
    }

    /**
     * @param $country
     * @return HTML_Head_Block
     */
    function setCountry($country) {

        $this->country = (string)$country;
        return $this;
    }

    /**
     * @param $keywords
     * @return HTML_Head_Block
     */
    function setKeywords($keywords) {

        $this->keywords = (array)$keywords;
        $this->keywords = implode(",", $this->keywords);

        return $this;
    }

    /**
     * @param $property
     * @return mixed
     */
    function __get($property) {

        return $this->$property;

    }

    /**
     *
     */
    function insert() {

        echo "
                  <meta content='text/html; charset=utf-8' http-equiv='Content-Type'>
                  <title>" . $this->title . "</title>
                  <meta content='" . $this->author . "' name='author'>
                  <meta content='" . $this->description . "' name='Description'>
                  <meta content='" . $this->keywords . "' name='Keywords'>
                  <meta content='" . $this->address . "' name='Geography'>
                  <meta content='English, Spanish, French, German, Italian, Japanese, Chinese' name='Language'>
                  <meta content='never' http-equiv='Expires'>
                  <meta content='" . $this->revisit . " days' name='Revisit-After'>
                  <meta content='Global' name='distribution'>
                  <meta content='" . $this->robot . "' name='Robots'>
                  <meta content='" . $this->country . "' name='country'>
                  <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js'></script>
                  <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js'></script>
               ";
    }

    /**
     *
     */
    function __destructor() {

        foreach (get_object_vars($this) as $key => $value) {

            unset($this->$key);

        }
    }
}
//$h = new HTML_Head_Block();
//$h->setAuthor('EW')->setDescription('This is my cool site')->insert();