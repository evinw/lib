<?php
/**
 * @author Evin Weissenberg
 */
class Copyright_Display {

    private $company_name;
    private $utf8;

    /**
     *
     */
    function __constructor() {

        $this->utf8 = ini_set('default_charset', 'UTF-8');

    }

    /**
     * @param $company_name
     * @return Copyright_Display
     */
    function setCompanyName($company_name) {

        $this->company_name = (string)$company_name;
        return $this;
    }

    /**
     * @param $property
     * @return mixed
     */
    function __get($property) {

        return $this->$property;

    }

    /**
     * @return string
     */
    function render() {

        $copyright = "Copyright © " . date('Y') . " " . $this->company_name;

        return $copyright;

    }

    /**
     *
     */
    function __destructor() {

        unset($this->utf8);
    }
}
//$c = new Copyright_Display();
//$c->setCompanyName('ACME LLC')->render();