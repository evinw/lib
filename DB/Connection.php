<?php
/**
 * @author Evin Weissenberg
 */

class Connection {

    public static $db = '1'; //1=production, 2=staging, 3=development.

    //PRODUCTION
    public static $host_1 = '';
    public static $username_1 = '';
    public static $password_1 = '';
    public static $db_name_1 = '';
    public static $port_1 = '';

    //STAGING
    public static $host_2 = '192.168.3.169';
    public static $username_2 = 'moc';
    public static $password_2 = 'm0cdev1';
    public static $db_name_2 = 'starz';
    public static $port_2 = '3306';

    //DEVELOPMENT
    public static $host_3 = 'localhost';
    public static $username_3 = 'root';
    public static $password_3 = 'b00gers';
    public static $db_name_3 = 'starzglobal';
    public static $port_3 = '3306';


    public static function dbConnection() {

        if (self::$db == 1) {

            mysql_connect(self::$host_1, self::$username_1, self::$password_1) or die(mysql_error());
            mysql_select_db(self::$db_name_1) or die(mysql_error());

            return true;

        } elseif (self::$db == 2) {

            mysql_connect(self::$host_2, self::$username_2, self::$password_2) or die(mysql_error());
            mysql_select_db(self::$db_name_2) or die(mysql_error());

            return true;

        } elseif (self::$db == 3) {

            mysql_connect(self::$host_3, self::$username_3, self::$password_3) or die(mysql_error());
            mysql_select_db(self::$db_name_3) or die(mysql_error());

            return true;


        } else {

            return false;

        }
    }
}


//Connection::$db=1;
//Connection::dbConnection();