<?php
/**
 * @author Evin Weissenberg
 */
class Build_Query_String {

    private $array;

    /**
     * @param $array
     * @return Build_Query_String
     */
    function setArray($array) {

        $this->array = (array)$array;
        return $this;

    }

    /**
     * @return string
     */
    function build() {

        $array = $this->array;
        $query_string = http_build_query($array) . "\n";

        return $query_string;

    }

    /**
     *
     */
    function __destruct() {

        unset($this->array);

    }
}
