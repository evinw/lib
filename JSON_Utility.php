<?php
/**
 * @author Evin Weissenberg
 */

class JSON_Utility {

    private $check_box = array();

    /**
     * @param $checkbox
     * @return JSON_Utility
     */
    function setCheckBox($checkbox) {

        $this->check_box = (array)$checkbox;
        return $this;

    }

    /**
     * @return string
     */
    function checkboxToJSON() {


        $json = array();

        foreach ($this->check_box as $key => $value) {


            if ($value == 'on') {

                array_push($json, $key);

            }

        }

        return $json = json_encode($json);

    }
}

?>