<?php
/**
 * @author Evin Weissenberg
 */
class Redirect {

    private $redirect_url;

    /**
     * @param $redirect_url
     * @return Redirect
     */
    function setRedirectUrl($redirect_url) {
        $this->redirect_url = $redirect_url;
        return $this;
    }

    function redirect() {

        //flush();
        header('Location: ' . $this->redirect_url);
        //exit();

    }
}