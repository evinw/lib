<?php
/**
 * @author Evin Weissenberg
 */
class JSON_Responder {

    private $code;
    private $type = 1; //1 Error, 2 Success, 3 Informative.
    private $message;
    private $data;

    /**
     * @todo add a more descriptive messages
     */
    const MESSAGE_TYPE_ERROR = 'Error';
    const MESSAGE_TYPE_SUCCESS = 'Success';
    const MESSAGE_TYPE_INFO = 'Informative';

    /**
     * @param $code
     * @return JSON_Responder
     */
    function setCode($code) {

        $this->code = (string)$code;
        return $this;

    }

    /**
     * @param $message
     * @return JSON_Responder
     */
    function setMessage($message) {

        $this->message = (string)$message;
        return $this;

    }

    /**
     * @param $date
     * @return JSON_Responder
     */
    function setData($date) {

        $this->data = (array)$date;
        return $this;
    }

    /**
     * @param $type
     * @return JSON_Responder
     * @throws Exception
     */
    function setType($type) {

        $this->type = (int)$type;

        try {

            if ($this->type == 1) {

                $this->type = self::MESSAGE_TYPE_ERROR;

            } elseif ($this->type == 2) {

                $this->type = self::MESSAGE_TYPE_SUCCESS;

            } elseif ($this->type == 3) {

                $this->type = self::MESSAGE_TYPE_INFO;

            } else {

                throw new Exception('Type must be options 1,2 or 3');

            }

        } catch (Exception $e) {

            echo $e->getMessage();

        }

        return $this;
    }

    /**
     * @param $property
     * @return mixed
     */
    function __get($property) {

        return $this->$property;

    }

    /**
     * @return string
     * @throws Exception
     */
    function respond() {

        try {

            if ($this->type == null) {

                throw new Exception('Property type is null.');

            } elseif ($this->code == null) {

                throw new Exception('Property code is null.');
            }
            elseif ($this->message) {

                throw new Exception('Property message is null.');

            } else {

                if ($this->data != NULL) {

                    // If there is data in the response this pattern will be used.
                    $response = array(
                        "Type" => $this->type,
                        "Code" => $this->code,
                        "Message" => $this->message,
                        "Data" => $this->data);

                    $response_json = json_encode($response);

                    return $response_json;

                } else {

                    // If there is no data in the response this pattern will be used.
                    $response = array(
                        "Type" => $this->type,
                        "Code" => $this->code,
                        "Message" => $this->message);

                    $response_json = json_encode($response);

                    return $response_json;
                }
            }

        } catch (Exception $e) {

            echo $e->getMessage();
            echo $e->getTrace();

        }
    }
}
//$j = new JSON_Responder();
//$j->setType(1)->setCode('304')->setMessage('Failed')->respond();