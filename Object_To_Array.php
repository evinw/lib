<?php
/**
 * @author Evin Weissenberg
 */
class Object_To_Array {
    /**
     * @param $obj
     * @return array
     */
    function toArray($obj) {
        if (is_object($obj)) $obj = (array)$obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = self::toArray($val);
            }
        } else {
            $new = $obj;
        }
        return $new;
    }


}