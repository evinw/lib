<?php
/**
 * @author Evin Weissenberg
 */
class Email_Sender {

    private $to;
    private $from;
    private $reply_to;
    private $subject;
    private $message;
    private $cc;
    private $bcc;
    private $support_email;
    private $success_message;
    private $failed_message;

    /**
     * @param $to
     * @return Email_Sender
     * @throws Exception
     */
    function setTo($to) {

        try {

            if (filter_var($to, FILTER_VALIDATE_EMAIL)) {

                $this->to = (string)$to;

            } else {

                throw new Exception('Not valid email.');

            }

        } catch (Exception $e) {


            echo $e->getMessage();
            return $this;


        }
    }

    /**
     * @param $from
     * @return Email_Sender
     */
    function setFrom($from) {

        $this->from = (string)$from;
        return $this;

    }

    /**
     * @param $reply_to
     * @return Email_Sender
     */
    function setReplyTo($reply_to) {

        $this->reply_to = (string)$reply_to;
        return $this;

    }

    /**
     * @param $subject
     * @return Email_Sender
     */
    function setSubject($subject) {

        $this->subject = (string)$subject;
        return $this;

    }

    /**
     * @param $message
     * @return Email_Sender
     */
    function setMessage($message) {

        $this->message = (string)$message;
        return $this;

    }

    /**
     * @param $cc
     * @return Email_Sender
     */
    function setCC($cc) {

        $this->cc = (string)$cc;
        return $this;

    }

    /**
     * @param $bcc
     * @return Email_Sender
     */
    function setBCC($bcc) {

        $this->bcc = (string)$bcc;
        return $this;

    }

    /**
     * @param $support_email
     * @return Email_Sender
     */
    function setSupportEmail($support_email) {

        $this->support_email = (string)$support_email;
        return $this;

    }

    /**
     * @param $success_message
     * @return Email_Sender
     */
    function setSuccessMessage($success_message) {

        $this->success_message = (string)$success_message;
        return $this;

    }

    /**
     * @param $failed_message
     * @return Email_Sender
     */
    function setFailedMessage($failed_message) {

        $this->failed_message = (string)$failed_message;
        return $this;

    }

    /**
     * @param $property
     * @return mixed
     */
    public function __get($property) {
        return $this->$property;
    }

    /**
     *
     */
    function send() {

        //Cache buster
        $random_hash = md5(date('r', time()));

        $to = $this->to;
        $subject = $this->subject;
        $message = $this->message;
        $headers = "From: " . $this->from . "\r\n";
        $headers .= "Reply-To: $this->reply_to\r\n";
        $headers .= "Return-Path: $this->reply_to\r\n";
        $headers .= "CC: $this->cc\r\n";
        $headers .= "BCC: $this->bcc\r\n";

        //Allows html emails
        $headers .= "Content-Type: multipart/alternative; boundary=\"PHP-alt-" . $random_hash . "\"";

        if (mail($to, $subject, $message, $headers)) {
            echo $this->success_message;
        } else {
            echo "$this->failed_message $this->support_email";
        }
    }
}
